﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication5.Models
{
    public class ProjectListDTO
    {
        public int id { set; get; }
        public string name { set; get; }

        public ProjectListDTO(Project project) {
            this.id = project.id;
            this.name = project.name;
        }
    }
}