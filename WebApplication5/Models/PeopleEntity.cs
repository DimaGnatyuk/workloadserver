﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication5.Models
{
    public class PeopleEntity
    {
        public int id { set; get; }
        public string name { set; get; }
        public int identificationKey { set; get; }
        public bool isDriver { set; get; }

        public object head { set; get; }
        public string vocation { set; get; }

        public ICollection<object> skillLanguage { set; get; }
        public ICollection<object> skillTechnologo { set; get; }
        public ICollection<object> userCertificate { set; get; }
        public ICollection<object> userWorkload { set; get; }
        public ICollection<object> userVisits { set; get; }

        public PeopleEntity(People people, DataClassesDataContext connect) {
            this.head = new HashSet<object>();
            this.skillLanguage = new HashSet<object>();
            this.userCertificate = new HashSet<object>();
            this.userWorkload = new HashSet<object>();
            this.userVisits = new HashSet<object>();

            this.id = people.id;
            this.name = people.name;
            this.identificationKey = people.identKey;
            this.isDriver = people.isDriver == 1 ? true: false;
            if (people.perent_id != null)
            {
                People h_people = connect.Peoples.Where(p => p.id == people.perent_id).First();
                if (h_people != null)
                {
                    var head_people = new
                    {
                        id = h_people.id,
                        name = h_people.name
                    };
                    this.head = head_people;
                }
            }
            if ((people.Vocation != null)&&(people.Vocation.name != null))
            {
                this.vocation = people.Vocation.name;
            }
            foreach (SkillLang skillLang in people.SkillLangs) {
                var lang = new {
                    language = skillLang.Language.name,
                    skill = skillLang.skill
                };
                this.skillLanguage.Add(lang);
            }

            foreach (SkillTech skillTech in people.SkillTeches) {
                var Tech = new
                {
                    technologe = skillTech.Technologe.name,
                    skill = skillTech.skill
                };
                this.skillTechnologo.Add(Tech);
            }

            foreach (UserCertificate certificate in people.UserCertificates) {
                var userCertificate = new {
                     certificate = certificate.Certificate.name,
                     key = certificate.number
                };
            }

            foreach (UserWorkload workload in people.UserWorkloads)
            {
                var Work = new
                {
                    project = workload.Project.name,
                    day1 = workload.day1,
                    day2 = workload.day2,
                    day3 = workload.day3,
                    day4 = workload.day4,
                    day5 = workload.day5,
                    day6 = workload.day6
                };
                this.userWorkload.Add(Work);
            }

            foreach (UserVisit uVisit in people.UserVisits) {
                var UVisite = new
                {
                    name = uVisit.Action.name,
                    coloe = uVisit.Action.color,
                    date = uVisit.data
                };
                this.userVisits.Add(UVisite);
            }
        }

    }
}