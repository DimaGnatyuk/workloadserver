﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication5.Models
{
    public class LanguagesEntity
    {
        public int id { set; get; }
        public string name { set; get; }

        public LanguagesEntity(Language language) {
            this.id = language.id;
            this.name = language.name;
        }
    }
}