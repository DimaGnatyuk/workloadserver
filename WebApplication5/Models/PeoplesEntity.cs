﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication5.Models
{
    public class PeoplesEntity
    {
        public int id { set; get;}
        public string name { set; get;}
        public string vocation { set; get; }
        public DateTime date { set; get; }

        public PeoplesEntity(People people)
        {
            this.id = people.id;
            this.name = people.name;
            if (people.Vocation != null)
            {
                this.vocation = people.Vocation.name;
            }
            this.date = people.startDate;
        }
    }
}