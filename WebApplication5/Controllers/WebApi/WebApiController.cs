﻿using Microsoft.ApplicationInsights.Extensibility.Implementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class WebApiController : ApiController
    {
        /*
         Method Peoples
         return Peoples
         Format id, name, vocation, data;
        */
        [ActionName("Peoples")]
        public async Task<ICollection<PeoplesEntity>> GetPeoples()
        {
            var connect = new DataClassesDataContext();
            ICollection<People> peoples = connect.Peoples.ToList();
            ICollection<PeoplesEntity> customArrayObject = new HashSet<PeoplesEntity>();
            foreach (People people in peoples)
            {
                PeoplesEntity peopleList = new PeoplesEntity(people);
                customArrayObject.Add(peopleList);
            }
            return await Task.FromResult(customArrayObject);
        }

        /*
         Method People
         return People By People.id 
         Format All Column;
        */
        [ActionName("People")]
        public async Task<PeopleEntity> GetPeopleById(int id)
        {
            var connect = new DataClassesDataContext();
            People people = connect.Peoples.Where(p => p.id == id).First();
            PeopleEntity peopleEntity = new PeopleEntity(people,connect);
            return await Task.FromResult(peopleEntity);
        }

        /*
         Method VocationList
         return All Vocation 
         Format id, name;
        */
        [ActionName("Vocations")]
        public async Task<ICollection<VacationListDTO>> GetVocations()
        {
            var connect = new DataClassesDataContext();
            ICollection<Vocation> vocations = connect.Vocations.ToList();
            ICollection<VacationListDTO> customArrayObject = new HashSet<VacationListDTO>();
            foreach (Vocation vocation in vocations)
            {
                VacationListDTO vocationDTO = new VacationListDTO();
                vocationDTO.id = vocation.id;
                vocationDTO.name = vocation.name;
                customArrayObject.Add(vocationDTO);
            }
            return await Task.FromResult(customArrayObject);
        }

        /*
         Method GetLanguageList
         return All Language 
         Format id, name;
        */
        [ActionName("Languages")]
        public async Task<ICollection<LanguagesEntity>> GetLanguageList()
        {
            var connect = new DataClassesDataContext();
            ICollection<Language> languages = connect.Languages.ToList();
            ICollection<LanguagesEntity> customArrayObject = new HashSet<LanguagesEntity>();
            foreach (Language language_o in languages)
            {
                LanguagesEntity language = new LanguagesEntity(language_o);
                customArrayObject.Add(language);
            }
            return await Task.FromResult(customArrayObject);
        }


        /*
         Method GetProjectList
         return All Project 
         Format id, name;
        */
        [ActionName("Projects")]
        public async Task<ICollection<ProjectListDTO>> GetProjectList()
        {
            var connect = new DataClassesDataContext();
            ICollection<Project> projects = connect.Projects.ToList();
            ICollection<ProjectListDTO> customArrayObject = new HashSet<ProjectListDTO>();
            foreach (Project project in projects)
            {
                ProjectListDTO progectList = new ProjectListDTO(project);
                customArrayObject.Add(progectList);
            }
            return await Task.FromResult(customArrayObject);
        }

        /*
        Method AddUser
        return Status insert into 
        Format Object JSON People;
       */
        [ActionName("AddUser")]
        public async Task<IHttpActionResult> PostAddUser(People people) {
            DataClassesDataContext connect = null;
            PeoplesEntity requestPeople = null;
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid type");
                }
                connect = new DataClassesDataContext();
                connect.Peoples.InsertOnSubmit(people);
                connect.SubmitChanges();
                people = connect.Peoples.Where(p => p.id == people.id).First();

                requestPeople = new PeoplesEntity(people);
            } catch (Exception error) {
                return BadRequest(error.Message);
                
            }
            return await Task.FromResult(Ok(requestPeople));
        }
    }
}
